<?php
/**
 * Created by PhpStorm.
 * User: zhaolei
 * Date: 2018/9/15
 * Time: 下午3:27
 */

/**
    数据库连接类
 **/

namespace zxl;

class Db{
    protected $pdo = null;
    protected $dbms='mysql';     //数据库类型
    protected $host='localhost'; //数据库主机名
    protected $dbName='zhaolei';    //使用的数据库
    protected $user='root';      //数据库连接用户名
    protected $pass='zhaolei';          //对应的密码


    public function Dbstart(){

        try {

            $db = new \PDO('mysql:dbname=zhaolei;host=127.0.0.1','root','zhaolei'); //初始化一个PDO对象

            return $db;


        } catch (PDOException $e) {

            die ("Error!: " . $e->getMessage() . "<br/>");

        }

    }


}
