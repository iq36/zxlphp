<?php
/**
 * Created by PhpStorm.
 * User: zhaolei
 * Date: 2018/9/10
 * Time: 下午5:23
 * 启动文件
 */
namespace zxl;

class zxl{

    public static $c;

    public function start(){
        include (ZXL.'function'.EX);
       self::$c = include(ZXL.'config'.EX);

        if(\zxl\zxl::$c['DEBUG']){  //是否开启DEBUG
            ini_set('display_errors','On');
            error_reporting(E_ALL);
        }

        spl_autoload_register('\zxl\zxl::myAutoLoad');

        $controller = isset($_GET['c'])?$_GET['c']:\zxl\zxl::$c['CONTROLLER'];

        $function = isset($_GET['a'])?$_GET['a']:\zxl\zxl::$c['FUNCTION'];

        $controller = '\App\Controller\\'.$controller.'Controller';

        $obj = new $controller;

        $obj->$function();


    }


    public static function myAutoLoad($className){

        $url = str_replace("\\","/",$className);
        if(@!include ($url.EX)){
         exit('文件不存在 或c不能当做参数');
        }


    }
}