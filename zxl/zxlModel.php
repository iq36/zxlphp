<?php
/**
 * Created by PhpStorm.
 * User: zhaolei
 * Date: 2018/9/25
 * Time: 上午9:48
 * 模型父类
 */

namespace zxl;

use zxl\Db;

class zxlModel extends Db{
    private $DB = null;
    private $table = 'test';
    private $FETCH_ASSOC = 'FETCH_ASSOC';
    private $field = '*';
    private $tablename = '';
    private $where     ="";
    private $order     ="";
    private $limit     ="";

    public function __construct(){
       $this->DB =  $this->Dbstart();
    }

    public function start(){

        return $this->DB;
    }

    public function table($tablename){
        isnull($tablename,'tablename()');
        $this->tablename = $tablename;
        return $this;
    }

    public function field($field = ''){
        isnull($field,'field()');
        $this->field = $field;
        return $this;

    }

    public function where($where= ''){
        isnull($where,'where()');

        $this->where = 'WHERE '.$where;
        return $this;
    }

    public function order($order = '',$desc = ''){
        isnull($order,'order()');
        $desc = empty($desc)?$desc = 'ASC':$desc;
        $this->order = 'ORDER BY '.$order.' '.$desc;
        return $this;
    }

    public function limit($limit=''){
        isnull($limit,'limit()');
        $this->limit = $limit;
        return $this;
    }


    public function select(){
        try {
            $res =  $this->DB->query("SELECT {$this->field} FROM {$this->tablename} {$this->where} {$this->order} ")->fetchAll(\PDO::FETCH_ASSOC);
            return $res;

        } catch (PDOException $e) {

            die ("Error!: " . $e->getMessage() . "<br/>");

        }

    }

}