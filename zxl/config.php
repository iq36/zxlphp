<?php
/**
 * Created by PhpStorm.
 * User: zhaolei
 * Date: 2018/9/10
 * Time: 下午5:28
 * 配置文件类
 */

return[
    'LOCALHOST'=>'127.0.0.1',
    'DEBUG'=>true,
    'CONTROLLER'=>'index',
    'FUNCTION'=>'index',
    'DBSTART'=>true,
];