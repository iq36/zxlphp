<?php
/**
 * Created by PhpStorm.
 * User: zhaolei
 * Date: 2018/9/21
 * Time: 下午5:36
 *
 * 控制器父类
 */

namespace zxl;


class zxlController
{

    public function error(){
        echo 'error';
    }

    public function success(){
        echo 'success';
    }


    public function get(){
        echo 'get';
    }

    public function post(){
        echo 'post';
    }

    /**
        获取所有get参数
     *
     **/
    public function GetAll(){
        $data = $_GET;
        return $this->filterWords($data);
    }

    /**
        获取指定get参数
     **/
    public function Getfirst($name = array()){
        $data = $_GET;
        $value = [];
        foreach ($name as $k=>$v){
            $value[$v] =  $data[$v];
        }
        return $this->filterWords($value);
    }


    /**
        过滤器
     **/

    public function filterWords($str)
      {
          $farr = array(
              "/<(\\/?)(script|i?frame|style|html|body|title|link|meta|object|\\?|\\%)([^>]*?)>/isU",
              "/(<[^>]*)on[a-zA-Z]+\s*=([^>]*>)/isU",
             "/select\b|insert\b|update\b|delete\b|drop\b|;|\"|\'|\/\*|\*|\.\.\/|\.\/|union|into|load_file|outfile|dump/is"
         );
          $arr = [];
          foreach ($str as $k=>$v){
              $str = preg_replace($farr,'',$v);
              $arr[$k] = strip_tags($str);
          }

         return $arr;
     }
}